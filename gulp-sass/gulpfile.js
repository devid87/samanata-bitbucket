var gulp = require('gulp');
var rename = require('gulp-rename');
var	sass    = require('gulp-sass');
var	plumber = require('gulp-plumber');
var concat  = require('gulp-concat');
var rename  = require('gulp-rename');
var clean   = require('gulp-clean-css');
var uglify  = require('gulp-uglify');
var autoprefixer  = require('gulp-autoprefixer');

gulp.task('styles', function() {
  gulp.src('src/sass/styles.scss')
  .pipe(plumber())
  .pipe(sass())
  .pipe(autoprefixer())
  //.pipe(gulp.dest('css')) // output style.css
  .pipe(sass({outputStyle: 'compressed'}))
  .pipe(rename('styles.min.css'))
  .pipe(gulp.dest('css')); // output style.min.css
});

gulp.task("watch", function() {
  gulp.watch(['src/sass/**/*.scss'], ['styles']);
});

gulp.task('default', ['styles', 'watch', ]);