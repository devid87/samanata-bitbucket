/*
 * Title:   Travelo - Travel, Tour Booking HTML5 Template - Custom Javascript file
 * Author:  http://themeforest.net/user/soaptheme
 */

(function($) {
    // UI Form Element
  $('.js-video img').click(function(){
  	  $(this).parent('.embed-video').addClass('play-video');
      var video = '<iframe class="embed-responsive-item" src="'+ $(this).attr('data-video') +'"></iframe>';
      $(this).replaceWith(video);

  });

  $(window).on("scroll",function() {
    if ($(this).scrollTop() > 200 ) {
      $('#back-to-top').fadeIn(400);
    } else {
      $('#back-to-top').fadeOut(400);
    }
  });
})(jQuery);