var gulp = require('gulp');
var rename = require('gulp-rename');
var	sass    = require('gulp-sass');
var	plumber = require('gulp-plumber');
var concat  = require('gulp-concat');
var rename  = require('gulp-rename');
var clean   = require('gulp-clean-css');
var uglify  = require('gulp-uglify');
var notify = require('gulp-notify');
var autoprefixer  = require('gulp-autoprefixer');
var merge = require('merge-stream');
var webpack = require('webpack');
var webpackStream = require('webpack-stream');
// Server
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var fs = require('fs');
var pkg = JSON.parse(fs.readFileSync('package.json'));

const serverConfig = {
	server: {
		baseDir: "./"
	},
	tunnel: false,
	host: 'localhost',
	port: 9000,
	logPrefix: "gulp_project"
};

gulp.task('styles', function() {
  return gulp.src(['./sass/style.scss'])
	.pipe(plumber({errorHandler: function(err){
		notify.onError({
			title: "Gulp Error in" + err.plugin,
			message: err.toString()
		})(err)
	}}))
  .pipe(sass())
  .pipe(autoprefixer())
  .pipe(gulp.dest('./css')) // output style.css
  .pipe(sass({outputStyle: 'compressed'}))
  .pipe(rename('style.min.css'))
  .pipe(plumber())
  .pipe(gulp.dest('./css')) // output style.min.css
  .pipe(reload({
		stream: true
	}));
});
gulp.task('styles-responsive', function() {
  return gulp.src(['./sass/responsive.scss'])
	.pipe(plumber({errorHandler: function(err){
		notify.onError({
			title: "Gulp Error in" + err.plugin,
			message: err.toString()
		})(err)
	}}))
  .pipe(sass())
  .pipe(autoprefixer())
  .pipe(gulp.dest('./css')) // output style.css
  .pipe(sass({outputStyle: 'compressed'}))
  .pipe(rename('responsive.min.css'))
  .pipe(plumber())
  .pipe(gulp.dest('./css')) // output style.min.css
  .pipe(reload({
		stream: true
	}));
});

gulp.task("watch", function() {
  gulp.watch(['./sass/**/*.scss'], ['styles', 'styles-responsive']);
});
gulp.task('webserver', function() {
	browserSync(serverConfig);
});

gulp.task('default', ['styles', 'styles-responsive', 'watch', 'webserver' ]);